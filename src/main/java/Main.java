/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * Exemplos de programação funcional
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * Adaptado de:
 * @author Speakjava (Simon Ritter)
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    /**
     * Execução dos exemplos.
     */
    public static void main(String[] args) {
        Main exemplos = new Main();
        exemplos.executa();
    }

    public void executa() {
        System.out.println("Exemplos de Lambdas e Streams");
        System.out.println("Executando Exemplo 1...");
        exemplo1();
        System.out.println("Executando Exemplo 2...");
        exemplo2();
        System.out.println("Executando Exemplo 3...");
        exemplo3();
        System.out.println("Executando Exemplo 4...");
        exemplo4();
        System.out.println("Executando Exemplo 5...");
        exemplo5();
    }

    /**
     * Exemplo 1
     *
     * Criar um String que consiste das primeiras letras de cada palavra
     * na lista de strings.
     */
    private void exemplo1() {
        List<String> list = Arrays.asList(
                "alpha", "bravo", "charlie", "delta", "echo", "foxtrot");

        StringBuilder sb = new StringBuilder();
        list.forEach(s -> sb.append(s.charAt(0)));
        String resultado = sb.toString();
        System.out.println("Exercício 1 resultado = " + resultado);
    }

    /**
     * Exemplo 2
     *
     * Remove da lista as palavras que têm coomprimento ímpar.
     */
    private void exemplo2() {
        List<String> list = new ArrayList<>(Arrays.asList(
                "alpha", "bravo", "charlie", "delta", "echo", "foxtrot"));

        list.removeIf(s -> (s.length() % 2) == 1);
        list.forEach(System.out::println);
    }

    /**
     * Exemplo 3
     *
     * Substitui cada elemento na lista pelo seu equivalente maiúsculo.
     */
    private void exemplo3() {
        List<String> list = new ArrayList<>(Arrays.asList(
                "alpha", "bravo", "charlie", "delta", "echo", "foxtrot"));
        list.replaceAll(String::toUpperCase);
        list.forEach(System.out::println);
    }

    /**
     * Exemplo 4
     *
     * Converte cada par chave-valor de um mapa em um String e os concatena.
     */
    private void exemplo4() {
        Map<String, Integer> map = new TreeMap<>();
        map.put("c", 3);
        map.put("b", 2);
        map.put("a", 1);

        StringBuilder sb = new StringBuilder();
        map.forEach((k, v) -> sb.append(String.format("%s%s", k, v)));
        String resultado = sb.toString();
        System.out.println("Exemplo 4 resultado = " + resultado);
    }

    /**
     * Exemplo 5
     *
     * Cria um novo Thread que exibe os números de uma lista.
     */
    private void exemplo5() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        new Thread(() -> list.forEach(System.out::println)).start();
    }
}
